const path = require('path');
const webpack = require("webpack");

module.exports = {
  entry: {
	  index: './src/index.js'
  },
  plugins: [],
  output: {
    filename: '[name].js',
    path: path.resolve(__dirname, './')
  },
  mode: 'development'
};
