function getElementScrollPosition( element = false ){
    
    if( !element ){ return false; }
    let elem = element.getBoundingClientRect();
    let toTop = elem.top;
    let scrollPos = window.pageYOffset;
    let elScroll = toTop + scrollPos;
    return elScroll;

}

function getScrollPositionBySelector( selector = false ){
    if( !selector ){ return 0; };
    
    let elements = document.querySelectorAll(selector);
    if( elements && elements.length ){
        let element = elements[0];
        return getElementScrollPosition(element);
    }

}

export function scrollTo( selector = false ){
    
    let elementScrollPos = getScrollPositionBySelector(selector);

    window.scroll({
        top: elementScrollPos, 
        left: 0, 
        behavior: 'smooth'
    });

}
